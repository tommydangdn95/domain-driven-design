﻿using Marketplace.Domain;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace Marketplace.Test
{
    public class ClassifiedAd_Publish_Spec
    {
        private readonly ClassifiedAd _classifiedAd;
        public ClassifiedAd_Publish_Spec()
        {
            this._classifiedAd = new ClassifiedAd(new ClassifiedAdId(Guid.NewGuid()), new UserId(Guid.NewGuid()));
        }

        [Fact]
        public void Can_publish_a_valid_ad()
        {
            this._classifiedAd.SetTitle(ClassifiedAdTitle.FromString("Test Ad"));
            this._classifiedAd.UpdateText(ClassifiedAdText.FromString("Please buy my stuff"));
            this._classifiedAd.UpdatePrice(Price.FromDecimal(100.10m, "EUR", new FakeCurrencyLookup()));
            this._classifiedAd.RequestToPublish();

            Assert.Equal(ClassifiedAd.ClassifiedAdState.PendingReview, this._classifiedAd.State);
        }

        [Fact]
        public void Cannot_publish_without_title()
        {
            this._classifiedAd.UpdateText(ClassifiedAdText.FromString("Please buy my stuff"));
            this._classifiedAd.UpdatePrice(Price.FromDecimal(100.10m, "EUR", new FakeCurrencyLookup()));
            Assert.Throws<InvalidEntityStateException>(() => this._classifiedAd.RequestToPublish());
        }

        [Fact]
        public void Cannot_publish_without_text()
        {
            this._classifiedAd.SetTitle(ClassifiedAdTitle.FromString("Test Ad"));
            this._classifiedAd.UpdatePrice(Price.FromDecimal(100.10m, "EUR", new FakeCurrencyLookup()));
            Assert.Throws<InvalidEntityStateException>(() => this._classifiedAd.RequestToPublish());
        }

        [Fact]
        public void Cannot_publish_without_Price()
        {
            this._classifiedAd.UpdateText(ClassifiedAdText.FromString("Please buy my stuff"));
            this._classifiedAd.SetTitle(ClassifiedAdTitle.FromString("Test Ad"));
            Assert.Throws<InvalidEntityStateException>(() => this._classifiedAd.RequestToPublish());
        }

        [Fact]
        public void Can_publish_a_valid_zero_price()
        {
            this._classifiedAd.SetTitle(ClassifiedAdTitle.FromString("Test Ad"));
            this._classifiedAd.UpdateText(ClassifiedAdText.FromString("Please buy my stuff"));
            this._classifiedAd.UpdatePrice(Price.FromDecimal(0.0m, "EUR", new FakeCurrencyLookup()));

            Assert.Throws<InvalidEntityStateException>(() => this._classifiedAd.RequestToPublish());
        }
    }
}
