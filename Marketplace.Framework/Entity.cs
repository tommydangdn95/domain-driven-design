﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Marketplace.Framework
{
    public abstract class Entity
    {
        private readonly List<object> _events;
        protected Entity() => _events = new List<object>();
        protected void Apply(object @event)
        {
            When(@event);
            EnsureValidState();
            this._events.Add(@event);
        }
        protected abstract void When(object @event);
        public IEnumerable<object> GetChanges() => this._events.AsEnumerable();
        public void ClearChanges() => this._events.Clear();
        protected abstract void EnsureValidState();
    }
}
