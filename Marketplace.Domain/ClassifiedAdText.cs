﻿using Marketplace.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace Marketplace.Domain
{
    public class ClassifiedAdText : Value<ClassifiedAdText>
    {
        private readonly string _value;

        public static ClassifiedAdText FromString(string text) => new ClassifiedAdText(text);

        internal ClassifiedAdText(string text) => _value = text;

        public static implicit operator string(ClassifiedAdText self) => self._value;
    }
}
